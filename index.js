// console.log("Welcome to Node.js Session!");

// HTTP 
// Use require directive keyword to import a module
// Creates an HTTP server using
const HTTP = require('http');

HTTP.createServer((request, response)=>{
    if(request.url === "/profile"){
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.write("Bad Request");
        response.end()
    }else if(request.url === "/greeting"){
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("This is my Homepage");
        response.end()
    }else{
        response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("This is the response from the request");
        response.end()
    }

}).listen(3000, () => console.log(`Server us connected to port ${PORT}`))


